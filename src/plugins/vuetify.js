import Vue from "vue";
import Vuetify from "vuetify/lib";
import "@mdi/font/css/materialdesignicons.css";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "mdi",
  },
  theme: {
    themes: {
      light: {
        primary: "#a6a6a6",
        secondary: "#76b83f",
        accent: "rgba(167,235,64,0.57)",
        error: "#FF0C3E",
        info: "#2196F3",
        success: "#5C941C",
        warning: "#FFC107",
      },
    },
    options: {
      customProperties: true,
    },
  },
});
