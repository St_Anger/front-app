import Vue from 'vue'
import vuetify from "./plugins/vuetify";
import App from './App.vue'
import axios from "axios";
import moment from "moment";
import endpoint from "./utils/endpoint";
import router from "./router";

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$endpoint = endpoint;
Vue.prototype.$moment = moment;

new Vue({
    router,
    vuetify,
  render: h => h(App),
}).$mount('#app')
